import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });

  // todo: show fighter info (image, name, health, etc.)
  const fighterImage = createFighterImage(fighter);
  const fighterInfo = createFighterInfo(fighter);
  console.log(fighterInfo);
  fighterElement.append(fighterImage, fighterInfo);
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { src: source };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    title: name,
    alt: name,
    attributes
  });
  return imgElement;
}


// My function

export function createFighterInfo(fighter) {
  const fighterInfo = createElement({
    tagName: 'div',
    className: 'fighter-preview___info'
  });
  console.log(fighterInfo);
  const elementsInfo = createElementsInfo(fighter);
  fighterInfo.append(...elementsInfo);

  return fighterInfo;
}


export function createElementsInfo(fighter) {
  const elementsInfo = Object.keys(fighter)
    .filter((element) => element !== '_id' && element !== 'source')
    .map((key) => {
      const element = createElement({
        tagName: 'div',
        className: `fighter-preview___${key}`,
        title: fighter[key]
      });
      element.innerHTML = `${key} - ${fighter[key]}`.toUpperCase();
      return element;
    });
  return elementsInfo;
}


