import { controls } from '../../constants/controls';


const MIN_CRITICAL_HIT_CHANCE = 1;
const MAX_CRITICAL_HIT_CHANCE = 2;

const BAR_BACKGROUND_COLOR = 'red';
const LEFT_POSITION = 'left';
const RIGHT_POSITION = 'right';

const {
  PlayerOneAttack: PLAYER_ONE_ATTACK,
  PlayerOneBlock: PLAYER_ONE_BLOCK,
  PlayerTwoAttack: PLAYER_TWO_ATTACK,
  PlayerTwoBlock: PLAYER_TWO_BLOCK,
  PlayerOneCriticalHitCombination: [KEY_Q, KEY_W, KEY_E],
  PlayerTwoCriticalHitCombination: [KEY_U, KEY_I, KEY_O]
} = controls;

let pressedPlayerOneAttack = false;
let pressedPlayerOneBlock = false;
let pressedPlayerTwoAttack = false;
let pressedPlayerTwoBlock = false;

const playerOneCriticalHitCombination = {
  usedCriticalHit: false,
  date: 0
};
const playerTwoCriticalHitCombination = {
  usedCriticalHit: false,
  date: 0
};

const criticalHitCombination = {
  KeyQ: false,
  KeyW: false,
  KeyE: false,
  KeyU: false,
  KeyI: false,
  KeyO: false
};
let hitKeysOnePlayer = new Set();
let hitKeysTwoPlayer = new Set();


export async function fight(firstFighter, secondFighter) {
  console.log('NEW GAME START!!!');
  let { health: firstFighterHealth } = firstFighter;
  let { health: secondFighterHealth } = secondFighter;

  let gameIsStart = true;
  return new Promise((resolve) => {
    document.addEventListener('keydown', (ev => {
      const { code } = ev;

      if (gameIsStart) {
        // if (controls.PlayerOneCriticalHitCombination.includes(code) && !criticalHitCombination[code]) {
        //   console.log('pressed - ', code);
        //   console.log('criticalHitCombination[code] - ', criticalHitCombination[code]);
        //   criticalHitCombination[code] = true;
        // }
        // if (controls.PlayerTwoCriticalHitCombination.includes(code) && !criticalHitCombination[code]) {
        //   console.log('pressed - ', code);
        //   criticalHitCombination[code] = true;
        // }
        // БЛОК ПЕРШОГО
        if (code === PLAYER_ONE_BLOCK && !pressedPlayerOneBlock) {
          console.log('pressed Player One Block');
          // pressedPlayerOneBlock = !pressedPlayerOneBlock;
          pressedPlayerOneBlock = true;
        }
        // БЛОК ДРУГОГО
        if (code === PLAYER_TWO_BLOCK && !pressedPlayerTwoBlock) {
          console.log('pressed Player Two Block');
          // pressedPlayerTwoBlock = !pressedPlayerTwoBlock;
          pressedPlayerTwoBlock = true;
        }
        // УДАР ПЕРШОГО
        if ((code === PLAYER_ONE_ATTACK) && !pressedPlayerOneAttack && !pressedPlayerOneBlock) {
          console.log('One player Attack!!!');
          if (!pressedPlayerTwoBlock) {
            let damage = getDamage(firstFighter, secondFighter);
            console.log('damage - ', damage);
            secondFighterHealth -= damage;
            console.log('secondFighterHealth - ', secondFighterHealth);
            updateHealthIndicator(RIGHT_POSITION, secondFighter, secondFighterHealth);
          }
          // pressedPlayerOneAttack = !pressedPlayerOneAttack;
          pressedPlayerOneAttack = true;
        }
        // УДАР ДРУГОГО
        if ((code === PLAYER_TWO_ATTACK) && !pressedPlayerTwoAttack && !pressedPlayerTwoBlock) {
          console.log('Two player Attack!!!');
          if (!pressedPlayerOneBlock) {
            let damage = getDamage(secondFighter, firstFighter);
            console.log('damage - ', damage);
            firstFighterHealth -= damage;
            console.log('firstFighterHealth - ', firstFighterHealth);
            updateHealthIndicator(LEFT_POSITION, firstFighter, firstFighterHealth);
          }
          // pressedPlayerTwoAttack = !pressedPlayerTwoAttack;
          pressedPlayerTwoAttack = true;
        }

        // КРИТИЧНИЙ УДАР ПЕРШОГО
        // if (checkCriticalHitCombinationOnePlayer()) {
        if (testCriticalHitOnePlayer(code) && !playerOneCriticalHitCombination.usedCriticalHit && (Date.now() - playerOneCriticalHitCombination.date) > 10000) {
          playerOneCriticalHitCombination.date = Date.now();
          playerOneCriticalHitCombination.usedCriticalHit = true;
          console.log('КРМТИЧНИЙ УДАР ПЕРШОГО');
          let damage = firstFighter.attack * 2;
          console.log('damage - ', damage);
          secondFighterHealth -= damage;
          console.log('secondFighterHealth - ', secondFighterHealth);
          updateHealthIndicator(RIGHT_POSITION, secondFighter, secondFighterHealth);
        }
        // КРМТИЧНИЙ УДАР ДРУГОГО
        // if (checkCriticalHitCombinationTwoPlayer()) {
        if (testCriticalHitTwoPlayer(code) && !playerTwoCriticalHitCombination.usedCriticalHit && (Date.now() - playerTwoCriticalHitCombination.date) > 10000) {
          playerTwoCriticalHitCombination.date = Date.now();
          playerTwoCriticalHitCombination.usedCriticalHit = true;
          console.log('КРМТИЧНИЙ УДАР ДРУГОГО');
          let damage = secondFighter.attack * 2;
          console.log('damage - ', damage);
          firstFighterHealth -= damage;
          console.log('secondFighterHealth - ', firstFighterHealth);
          updateHealthIndicator(LEFT_POSITION, firstFighter, firstFighterHealth);
        }

        // СМЕРТЬ ПЕРШОГО
        if (firstFighterHealth <= 0) {
          firstFighterHealth = firstFighter.health;
          gameIsStart = false;
          resolve(secondFighter);
        }
        // СМЕРТЬ ДРУГОГО
        if (secondFighterHealth <= 0) {
          secondFighterHealth = secondFighter.health;
          console.log('RESOLVE WORK');
          console.log(secondFighterHealth);
          gameIsStart = false;
          resolve(firstFighter);
        }
      }
    }), false);

    document.addEventListener('keyup', keyUpHandler, false);

    // resolve the promise with the winner when fight is over
  });
}

// export function getDamage(attacker, defender) {
//   // return damage
//   const damage = (attacker - defender) < 0 ? 0 : attacker - defender;
//   return damage;
// }

// ДРУГИЙ ВАРІАНТ
export function getDamage(attacker, defender) {
  // return damage
  let hitPower = getHitPower(attacker);
  let blockPower = getBlockPower(defender);
  const damage = (hitPower - blockPower) < 0 ? 0 : (hitPower - blockPower);
  console.log('hitPower - ', hitPower);
  console.log('blockPower - ', blockPower);
  console.log('damage - ', damage);
  return damage;
}

// export function getHitPower(fighter) {
//   // return hit power
//   const { attack } = fighter;
//   const hitPower = attack * getCriticalHitChance(MIN_CRITICAL_HIT_CHANCE, MAX_CRITICAL_HIT_CHANCE);
//   return hitPower;
// }

export function getHitPower(fighter) {
  // return hit power
  const { attack } = fighter;
  let criticalHitChance = Math.random() * (MAX_CRITICAL_HIT_CHANCE - MIN_CRITICAL_HIT_CHANCE) + MIN_CRITICAL_HIT_CHANCE;
  console.log('criticalHitChance - ', criticalHitChance);
  const hitPower = attack * criticalHitChance;
  return hitPower;
}

export function getBlockPower(fighter) {
  // return block power
  const { defense } = fighter;
  let dodgeChance = Math.random() * (MAX_CRITICAL_HIT_CHANCE - MIN_CRITICAL_HIT_CHANCE) + MIN_CRITICAL_HIT_CHANCE;
  console.log('dodgeChance - ', dodgeChance);
  const blockPower = defense * dodgeChance;
  return blockPower;
}

// My functions

// function getCriticalHitChance(min, max) {
//   min = Math.ceil(min);
//   max = Math.floor(max);
//   return Math.random() * (max - min + 1) + min;
// }

export function keyUpHandler(event) {
  const { code } = event;
  if (code === PLAYER_TWO_BLOCK) {
    console.log('pressed Player Two STOP Block');
    // pressedPlayerTwoBlock = !pressedPlayerTwoBlock;
    pressedPlayerTwoBlock = false;
  }
  if (code === PLAYER_ONE_BLOCK) {
    console.log('pressed Player One STOP Block');
    // pressedPlayerOneBlock = !pressedPlayerOneBlock;
    pressedPlayerOneBlock = false;
  }
  if (code === PLAYER_ONE_ATTACK) {
    console.log('One player STOP Attack!!!');
    pressedPlayerOneAttack = false;
  }
  if (code === PLAYER_TWO_ATTACK) {
    console.log('Two player STOP Attack!!!');
    pressedPlayerTwoAttack = false;
  }
  if (controls.PlayerOneCriticalHitCombination.includes(code)) {
    console.log('up - ', code);
    criticalHitCombination[code] = false;
    playerOneCriticalHitCombination.usedCriticalHit = false;
    console.log('playerOneCriticalHitCombination - ', playerOneCriticalHitCombination);


    hitKeysOnePlayer.delete(code);
  }
  if (controls.PlayerTwoCriticalHitCombination.includes(code)) {
    console.log('up - ', code);
    criticalHitCombination[code] = false;
    playerTwoCriticalHitCombination.usedCriticalHit = false;
    console.log('playerTwoCriticalHitCombination - ', playerTwoCriticalHitCombination);


    hitKeysTwoPlayer.delete(code);
  }


}

function updateHealthIndicator(position, fighter, fighterHealth) {
  const bar = document.getElementById(`${position}-fighter-indicator`);
  const healthFighter = (fighterHealth / fighter.health) * 100;
  bar.style.width = `${healthFighter}%`;
  if (healthFighter <= 20) {
    bar.style.backgroundColor = BAR_BACKGROUND_COLOR;
  }
  if (healthFighter <= 0) {
    bar.style.width = '0';
  }
}

function checkCriticalHitCombinationOnePlayer() {
  return !!(criticalHitCombination[KEY_Q]
    && criticalHitCombination[KEY_W]
    && criticalHitCombination[KEY_E]
    && !playerOneCriticalHitCombination.usedCriticalHit
    && (Date.now() - playerOneCriticalHitCombination.date) > 10000);
}

function checkCriticalHitCombinationTwoPlayer() {
  return !!(criticalHitCombination[KEY_U]
    && criticalHitCombination[KEY_I]
    && criticalHitCombination[KEY_O]
    && !playerTwoCriticalHitCombination.usedCriticalHit
    && (Date.now() - playerTwoCriticalHitCombination.date) > 10000);
}

function testCriticalHitOnePlayer(code) {
  // console.log(hitKeysOnePlayer);
  if (controls.PlayerOneCriticalHitCombination.includes(code)) {
    hitKeysOnePlayer.add(code)
  }
  return hitKeysOnePlayer.size === 3;
}

function testCriticalHitTwoPlayer(code) {
  // console.log(hitKeysTwoPlayer);
  if (controls.PlayerTwoCriticalHitCombination.includes(code)) {
    hitKeysTwoPlayer.add(code)
  }
  return hitKeysTwoPlayer.size === 3;
}






