import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  let title = `And the winner is - ${fighter.name}!`;
  let bodyElement = createElement({tagName:'div', className: 'winner__body'});
  showModal({title, bodyElement})
  // call showModal function 
}
